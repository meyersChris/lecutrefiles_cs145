//    10/01/18
//
//    Review of CS145 Day 2.
import java.util.Arrays;

public class day2Review {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(args));
        int[] nums = {4, 5, 9, 32, 7, -14};
        System.out.println(average(nums));

        for (int i : nums) {
            System.out.printf("Hi %7d is your number.\n", i);
        }

        System.out.println(Math.tan(26.54));
    }


    public static int average(int[] numbers){
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum / numbers.length;
    }
}
